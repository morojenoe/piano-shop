import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatDividerModule } from "@angular/material/divider";
import { HttpClientModule } from "@angular/common/http";
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from "@angular/material/sidenav"
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { NgStringPipesModule, NgArrayPipesModule } from 'angular-pipes';
import { AlertModule } from 'ngx-bootstrap';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { AppComponent } from './app.component';
import { PianoListComponent } from './piano-list/piano-list.component';
import { PianoDescriptionComponent } from './piano-description/piano-description.component';
import { PianoService } from "./piano.service";
import { AppRoutingModule } from './/app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    PianoListComponent,
    PianoDescriptionComponent
  ],
  imports: [
    BrowserModule,
    MatDividerModule,
    MatListModule,
    MatSidenavModule,
    MatGridListModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    NgStringPipesModule,
    NgArrayPipesModule,
    HttpClientModule,
    AlertModule.forRoot(),
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    AppRoutingModule
  ],
  providers: [PianoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
