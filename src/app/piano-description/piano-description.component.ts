import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Piano } from '../piano';
import { PianoService } from '../piano.service';


@Component({
  selector: 'app-piano-description',
  templateUrl: './piano-description.component.html',
  styleUrls: ['./piano-description.component.css']
})
export class PianoDescriptionComponent implements OnInit {
  piano: Piano;
  mainPicUrl: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private pianoService: PianoService
  ) {}

  ngOnInit() {
    this.getPiano();
  }

  getPiano(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.pianoService.getPiano(id).subscribe(_piano => {
      this.piano = _piano;
      this.mainPicUrl = this.piano.pictures[0];
    });
  }

  showPicture(pictureUrl: string): void {
    this.mainPicUrl = pictureUrl;
  }

  addToCart(piano: Piano): void {
    this.pianoService.addToCart(piano);
  }

}
