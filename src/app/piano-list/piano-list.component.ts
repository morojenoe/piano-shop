import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Piano } from '../piano';
import { PianoService } from "../piano.service";
import { TruncatePipe } from 'angular-pipes';
import { Observer } from 'rxjs/Observer';
import { ArrayObservable } from 'rxjs/observable/ArrayObservable';

@Component({
  selector: 'app-piano-list',
  templateUrl: './piano-list.component.html',
  styleUrls: ['./piano-list.component.css'],
})
export class PianoListComponent implements OnInit {
  pianos: Piano[];
  isInsideCart: boolean;

  constructor(private route: ActivatedRoute,
    private location: Location,
    private pianoService: PianoService) {
  }

  ngOnInit() {
    this.isInsideCart = this.route.snapshot.url[0].path === 'cart';
    this.getPianos();
  }

  getPianos() {
    if (this.isInsideCart == true) {
      return this.pianoService.getCart().subscribe(pianos => this.pianos = pianos);
    } else {
      return this.pianoService.getPianos().subscribe(pianos => this.pianos = pianos);
    }
  }

  addToCart(piano: Piano): void {
    this.pianoService.addToCart(piano);
  }

  removeFromCart(piano: Piano): void {
    this.pianoService.removeFromCart(piano);
    this.getPianos();
  }

  filterList(searchStr: string): void {
    if (searchStr === undefined || searchStr === "") {
      this.getPianos();
    } else {
      let subscription = this.getPianos();
      subscription.add(_ => this.pianos = this.pianos.filter(piano => piano.model.search(searchStr) != -1));
    }
  }
}
