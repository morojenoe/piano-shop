import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';

import { PianoListComponent } from './piano-list/piano-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(PianoListComponent) private pianoListComponent: PianoListComponent;

  search(searchStr): void {
    this.pianoListComponent.filterList(searchStr);
  }

  onActivate(childComponent: any): void {
    this.pianoListComponent = childComponent;
  }
}
