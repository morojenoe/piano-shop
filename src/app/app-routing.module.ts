import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from "./app.component";
import { PianoListComponent } from './piano-list/piano-list.component';
import { PianoDescriptionComponent } from './piano-description/piano-description.component';

const routes: Routes = [
  { path: '', redirectTo: '/pianos', pathMatch: 'full' },
  { path: 'pianos', component: PianoListComponent },
  { path: 'piano/:id', component: PianoDescriptionComponent },
  { path: 'cart', component: PianoListComponent }
];

@NgModule({
  exports: [ 
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule {}