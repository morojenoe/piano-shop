export class Piano {
  id: Number;
  pictures: string[];
  description: string;
  model: string;
  cost: Number;
  level: string;
  weight: Number;
  dimensions: string;
}
