import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Piano } from "./piano";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PianoService {
  pianoUrl = "api/pianos";
  cart: Piano[];

  constructor(private http: HttpClient) {
    console.log('I am in PianoService');
    this.cart = [];
  }

  getPianos(): Observable<Piano[]> {
    return this.http.get<Piano[]>(this.pianoUrl);
  }

  getPiano(id: Number): Observable<Piano> {
    return this.http.get<Piano>(this.pianoUrl + `/${id}`);
  }

  getCart(): Observable<Piano[]> {
    return of(this.cart);
  }

  addToCart(piano: Piano): void {
    if (this.cart.find(cartPiano => cartPiano.id == piano.id) === undefined) {
      this.cart.push(piano);
    }
  }

  removeFromCart(piano: Piano): void {
    let index = this.cart.findIndex(carPiano => carPiano.id === piano.id);
    if (index > -1) {
      this.cart.splice(index, 1);
    }
  }  
}
